package java.configuration;

import java.util.HashMap;
import java.util.Map;


public class HttpRequestConfiguration<P, R> extends RequestConfiguration<P, R> {

    // The ONLY option
    public static final String METHOD_POST = "POST";

    /**
     * Хост бекенда
     */
    private String host;

    /**
     * Http метод
     */
    private String method;

    /**
     * Длина чанка при отправке запроса
     */
    private int chunkLength;

    /**
     * Заголовки по умолчанию
     */
    private Map<String, String> headers = new HashMap<>();


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getChunkLength() {
        return chunkLength;
    }

    public void setChunkLength(int chunkLength) {
        this.chunkLength = chunkLength;
    }

    public void setHeader(String name, String value) {
        headers.put(name, value);
    }

    public String getHeader(String name) {
        return headers.get(name);
    }

    public void removeHeader(String name) {
        headers.remove(name);
    }

    public boolean hasHeader(String name) {
        return headers.containsKey(name);
    }

    public Iterable<String> getHeaders() {
        return headers.keySet();
    }

}
