package java.configuration;

import java.request.Request;
import java.request.RequestProcessor;
import java.request.RequestResult;

import java.util.ArrayList;
import java.util.List;



/**
 * Конфигурация отправителя запросов
 * @param <P> тип тела запроса
 * @param <R> тип тела ответа
 */
public abstract class RequestConfiguration<P, R> {

    /**
     * Список обработчиков состояний
     */
    private List<RequestProcessor<P, R>> requestProcessors = new ArrayList<>();


    public void addProcessor(RequestProcessor<P, R> requestProcessor) {
        requestProcessors.add(requestProcessor);
    }

    public void removeProcessor(RequestProcessor<P, R> requestProcessor) {
        requestProcessors.remove(requestProcessor);
    }

    /**
     * Возвращает подходящий под указанный запрос обработчик
     */
    public RequestProcessor<P, R> getProcessor(Request request, RequestResult<R, P> result) {
        for (RequestProcessor<P, R> requestProcessor: requestProcessors) {
            if (requestProcessor.is(request, result)) {
                return requestProcessor;
            }
        }

        return null;
    }

}
