package java.store;


public abstract class HttpRequestStore implements RequestStore {

    public static final String KEY_ACCESS_TOKEN = "access_token";

    public String getAccessToken() {
        return get(KEY_ACCESS_TOKEN);
    }

    public void setAccessToken(String token) {
        set(KEY_ACCESS_TOKEN, token);
    }

}
