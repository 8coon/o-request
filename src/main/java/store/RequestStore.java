package java.store;


/**
 * Хранилище данных, сохраняемое между запросами
 */
public interface RequestStore {

    /**
     * Получить значение по ключу
     */
    String get(String key);

    /**
     * Записать значение по ключу
     */
    String set(String key, String value);

    /**
     * Загружает данные из постоянного хранилища
     */
    void sync();

    /**
     * Сохраняет данные в постоянном хранилище
     */
    void persist();

}
