package java.request.rpc;

import java.configuration.HttpRequestConfiguration;
import java.configuration.RequestConfiguration;
import java.request.RequestAuthenticator;
import java.request.RequestLogger;
import java.request.RequestProcessor;
import java.request.RequestResult;
import java.request.exception.NoAccessTokenException;
import java.store.HttpRequestStore;
import java.store.RequestStore;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


public abstract class HttpRequest<P, A extends AuthorizedPayload, R> extends AuthRequest<P, A, R> {

    public static final int STATUS_UNDEFINED = -1;

    private HttpRequestStore store;
    private HttpRequestConfiguration<P, R> configuration;
    private RequestLogger<P, RequestResult<R, P>> logger;


    public HttpRequest(HttpRequestStore requestStore, RequestLogger<P, RequestResult<R, P>> requestLogger) {
        store = requestStore;
        logger = requestLogger;
        configuration = new HttpRequestConfiguration<P, R>();
    }

    /**
     * Клонируем объект, задавая новый scope для логгера
     * @param scope для логгера
     */
    public abstract HttpRequest<P, A, R> cloneWithScope(String scope);

    /**
     * Делаем запрос, не треующий авторизации
     */
    @Override
    protected RequestResult<R, P> callUnauthorized(String path, P payload) {
        RequestResult<R, P> result = this.makeRequest(path, payload);

        // Находим подходящий обработчик запроса
        RequestProcessor<P, R> processor = configuration.getProcessor(this, result);

        // Не нашли
        if (processor == null) {
            // Лог
            logger.onNoRequestProcessor(path, payload, result);
            return result;
        }

        // Лог
        logger.onBeforeRequestProcessor(path, payload, processor);

        // Нашли и выполнили
        result = processor.exec(this, result);

        // Лог
        logger.onAfterRequestProcessor(path, payload, processor, result);
        return result;
    }

    /**
     * Делаем запрос с авторизацией
     */
    @Override
    protected RequestResult<R, P> callAuthorized(String path, A payload) {
        String token = store.getAccessToken();

        // Если нет токена, выбросим исключение, его потом обработает нужный RequestProcessor
        if (token == null) {
            throw new NoAccessTokenException();
        }

        payload.setAccessToken(store.getAccessToken());
        return this.callUnauthorized(path, (P) payload);
    }


    @Override
    public abstract RequestAuthenticator getAuthenticator();

    @Override
    public RequestStore getStore() {
        return this.store;
    }

    @Override
    public RequestConfiguration<P, R> getConfiguration() {
        return this.configuration;
    }

    protected abstract void writePayload(OutputStream stream, P payload) throws Exception;
    protected abstract R loadResponse(InputStream stream) throws Exception;

    /**
     * Тут делаем сам Http-запрос
     */
    private RequestResult<R, P> makeRequest(String path, P payload) {
        // Лог
        logger.onBeforeRequest(path, payload);

        R response = null;
        Throwable throwable = null;
        HttpURLConnection connection = null;
        int status = STATUS_UNDEFINED;
        URL url;

        try {
            // Открываем и настраиваем соединение
            url = new URL(configuration.getHost() + path);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(configuration.getChunkLength());
            connection.setRequestMethod(configuration.getMethod());

            for (String name: configuration.getHeaders()) {
                connection.setRequestProperty(name, configuration.getHeader(name));
            }

            // Отправляем тело запроса
            OutputStream out = new BufferedOutputStream(connection.getOutputStream());
            writePayload(out, payload);

            // Загружаем и парсим тело ответа
            status = connection.getResponseCode();
            response = loadResponse(new BufferedInputStream(connection.getInputStream()));
        } catch (Exception e) {
            throwable = e;

            // Лог плохо
            logger.onFailedRequest(path, payload, e);
        } finally {
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();

                    // Лог плохо
                    logger.onFailedRequest(path, payload, e);
                }
            }
        }

        RequestResult<R, P> result = new RequestResult<>(response, payload, status, throwable);

        // Лог
        logger.onAfterRequest(path, payload, result);
        return result;
    }

}
