package java.request.rpc;

import java.request.Request;
import java.request.RequestResult;

/**
 * Отправитель запросов, который умеет отделять запросы с авторизацией и без
 * @param <P> тип тела запроса без авторизации
 * @param <A> тип тела запроса с авторизацией
 * @param <R> тип тела ответа
 */
public abstract class AuthRequest<P, A extends AuthorizedPayload, R> implements Request<P, R> {

    @Override
    @SuppressWarnings("unchecked")
    public RequestResult<R, P> call(String path, P payload) {
        if (payload == null) {
            return this.callUnauthorized(path, null);
        }

        // С авторизацией
        if (getAuthPayloadClass().isAssignableFrom(payload.getClass())) {
            return this.callAuthorized(path, (A) payload);
        }

        // Без авторизации
        return this.callUnauthorized(path, payload);
    }

    protected abstract RequestResult<R, P> callUnauthorized(String path, P payload);
    protected abstract RequestResult<R, P> callAuthorized(String path, A payload);
    protected abstract Class<A> getAuthPayloadClass();

}
