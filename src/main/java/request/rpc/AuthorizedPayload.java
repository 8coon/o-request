package java.request.rpc;

public interface AuthorizedPayload {

    void setAccessToken(String token);

}
