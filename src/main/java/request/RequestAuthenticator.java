package java.request;


public interface RequestAuthenticator {

    String getAuthToken(String accountName) throws Exception;

}
