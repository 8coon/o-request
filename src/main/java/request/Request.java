package java.request;


import java.configuration.RequestConfiguration;
import java.store.RequestStore;

/**
 * Отправитель запросов
 * @param <P> Тип тела запроса
 * @param <R> Тип тела ответа
 */
public interface Request<P, R> {

    RequestResult<R, P> call(String path, P payload);

    RequestAuthenticator getAuthenticator();
    RequestStore getStore();
    RequestConfiguration getConfiguration();

}
