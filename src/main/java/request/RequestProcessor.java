package java.request;

/**
 * Интерфейс обработчика статуса ответа
 * @param <P> тип тела запроса
 * @param <R> тип тела ответа
 * */
public interface RequestProcessor<P, R> {

    /**
     * Нужно ли использовать этот процессор для данного ответа сервера?
     */
    boolean is(Request request, RequestResult<R, P> result);

    /**
     * Синхронно обработать состояние
     */
    RequestResult<R, P> exec(Request request, RequestResult<R, P> result);

}
