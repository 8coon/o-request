package java.request;

/**
 * Результат запроса
 * @param <R> тип тела ответа
 * @param <P> тип тела запроса
 */
public class RequestResult<R, P> {

    /**
     * Ответ
     */
    private R response;

    /**
     * Запрос
     */
    private P payload;

    /**
     * Код ответа
     */
    private int status;

    /**
     * Исключение, если было выброшено во время выполнения запроса
     */
    private Throwable throwable;


    public RequestResult(
            R response,
            P payload,
            int status,
            Throwable throwable
    ) {
        this.response = response;
        this.payload = payload;
        this.status = status;
        this.throwable = throwable;
    }

    public R getResponse() {
        return response;
    }

    public int getStatus() {
        return status;
    }

    public P getPayload() {
        return payload;
    }

    public Throwable getThrowable() {
        return throwable;
    }

}
