package java.request;

public interface RequestLogger<P, R> {

    void onBeforeRequest(String path, P payload);
    void onNoRequestProcessor(String path, P payload, R result);
    void onBeforeRequestProcessor(String path, P payload, RequestProcessor processor);
    void onAfterRequestProcessor(String path, P payload, RequestProcessor processor, R result);
    void onFailedRequest(String path, P payload, Exception e);
    void onAfterRequest(String path, P payload, R result);

    RequestLogger<P, R> cloneWithScope(String scope);

}
